package br.com.veere.csv;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import br.com.veere.entity.Animal;
import br.com.veere.entity.Variedade;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by icc on 05/01/2017.
 */
public class CSVDataHandler implements Serializable {

    private boolean isVariedade;

    public List<String[]> preview(Reader reader) {
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> list = null;
        try {
            list = csvReader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Animal> converterParaListaAnimal(List<String[]> entrada) {
        List<Animal> retorno = new ArrayList<>();
        Date dataConvertida = null;
        Variedade variedade = null;
        String regexData = "^[0-3]?[0-9]/[0-3]?[0-9]/(?:[0-9]{2})?[0-9]{2}$";
        Pattern patternData = Pattern.compile(regexData);
        for (String[] linha : entrada) {
            try {
                Matcher matcherData = patternData.matcher(linha[1]);
                if (matcherData.matches()) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    dataConvertida = sdf.parse(linha[1]);
                }
                isVariedade = Variedade.values().toString().toLowerCase().contains(linha[2].toLowerCase());
                if (isVariedade) {
                    variedade = Variedade.valueOf(linha[2]);
                }
            } catch (ParseException e) {
                continue;
            }
            retorno.add(new Animal(null, linha[0], dataConvertida, variedade, linha[3]));
        }
        return retorno;
    }


    public List<Animal> converter(Reader reader) {
        CSVReader csvReader = new CSVReader(reader);
//        CsvToBean csv = new CsvToBean();
        //Set column mapping strategy
//        List<Animal> list = csv.parse(setColumMapping(), csvReader);
        try {
            List<String[]> list = csvReader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String test() {
        return "test ok";
    }

    private static ColumnPositionMappingStrategy setColumMapping() {
        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(Animal.class);
        String[] columns = new String[]{
                "apelido", "dataNascimento", "raca", "variedade"
        };
        strategy.setColumnMapping(columns);
        return strategy;
    }

}
