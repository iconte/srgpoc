package br.com.veere.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by icc on 02/01/2017.
 */
public class Animal implements Serializable{
    private Long id;
    private String apelido;
    private Date dataNascimento;
    private Variedade variedade;
    private String raca;

    public Animal(Long id, String apelido, Date dataNascimento, Variedade variedade, String raca) {
        this.id = id;
        this.apelido = apelido;
        this.dataNascimento = dataNascimento;
        this.variedade = variedade;
        this.raca = raca;
    }

    public Animal() {
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Variedade getVariedade() {
        return variedade;
    }

    public void setVariedade(Variedade variedade) {
        this.variedade = variedade;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        if (!id.equals(animal.id)) return false;
        if (apelido != null ? !apelido.equals(animal.apelido) : animal.apelido != null) return false;
        if (dataNascimento != null ? !dataNascimento.equals(animal.dataNascimento) : animal.dataNascimento != null)
            return false;
        if (variedade != null ? !variedade.equals(animal.variedade) : animal.variedade != null) return false;
        return raca != null ? raca.equals(animal.raca) : animal.raca == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (apelido != null ? apelido.hashCode() : 0);
        result = 31 * result + (dataNascimento != null ? dataNascimento.hashCode() : 0);
        result = 31 * result + (variedade != null ? variedade.hashCode() : 0);
        result = 31 * result + (raca != null ? raca.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", apelido='" + apelido + '\'' +
                ", dataNascimento=" + dataNascimento +
                ", variedade='" + variedade + '\'' +
                ", raca='" + raca + '\'' +
                '}';
    }
}
