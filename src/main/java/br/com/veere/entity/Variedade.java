package br.com.veere.entity;

/**
 * Created by icc on 03/01/2017.
 */
public enum Variedade {
    VERMELHO_BRANCO("Vermelho e Branco"), PRETO_BRANCO("Preto e Branco");

    private String variedade;

    Variedade(String variedade) {
        this.variedade = variedade;
    }

    public String getDescricao() {
        return variedade;
    }
}
