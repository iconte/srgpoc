package br.com.veere.bean;

import br.com.veere.csv.CSVDataHandler;
import br.com.veere.entity.Animal;
import br.com.veere.entity.Variedade;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by icc on 02/01/2017.
 */
@ManagedBean
@ViewScoped
public class AnimaisBean implements Serializable {

    private List<Animal> animais = null;
    private Animal animal = new Animal();
    private List<String> apelidos = null;
    private boolean deveFazerUpload = false;
    private boolean deveExibirBotaoPreview = false;
    private UploadedFile arquivo;
    private List<String[]> preview;


    public void init() {
        if (this.getAnimais() == null) {
//            this.animais = new ArrayList<>();
//            for (int i = 1; i <= 100; i++) {
//                Animal animal = new Animal(new Long(i), "Ze Mayer" + i, new Date(), i % 2 == 0 ? Variedade.PRETO_BRANCO : Variedade.VERMELHO_BRANCO, "Raça " + i);
//                this.animais.add(animal);
//            }
            apelidos = gerarApelidos();
        }
    }

    public AnimaisBean() {
        init();
    }

    public void salvar() {
        if (this.animais == null) {
            this.animais = new ArrayList<>();
        }
//        System.out.println(animal.toString());
        this.animais.add(animal);
        this.animal = new Animal();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Sucesso.", "Adicionado com sucesso."));
    }

    public void executarImportacao() {
        try {
            if (this.preview != null) {
                List<Animal> listaConvertida = new CSVDataHandler().converterParaListaAnimal(preview);
                if(this.getAnimais()==null){
                    this.setAnimais(listaConvertida);
                }else{
                    this.getAnimais().addAll(listaConvertida);
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Sucesso.", "Importou com sucesso."));
                this.preview = null;
                this.deveExibirBotaoPreview = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Vazio.", "preview vazio"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Vazio.", "preview vazio"));
        }
    }

    public void executarUpload(FileUploadEvent event) throws IOException {
        try {
            UploadedFile arquivoRecebido = event.getFile();
            if (arquivoRecebido == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Info.", "Nao abriu!"));
            } else {
                this.arquivo = arquivoRecebido;
                ByteArrayInputStream bis = new ByteArrayInputStream(arquivoRecebido.getContents());
                InputStreamReader ios = new InputStreamReader(bis);
                this.preview = new CSVDataHandler().preview(ios);
//                if(preview!=null){
//                    RequestContext context = RequestContext.getCurrentInstance();
//                    context.execute("PF('myDialogVar').show();");
//                }
//                deveExibirBotaoPreview = true;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Info.", arquivoRecebido.getFileName()));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Erro", "Erro no upload:" + e.getMessage()));
        }
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Alterado :", "Antes: " + oldValue + ", Depois:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            System.out.println(newValue.toString());
        }
    }

    public List<String> completeApelido(String query) {
        List<String> sugestoes = new ArrayList<>();
        for (String apelido : getApelidos()) {
            if (apelido.toLowerCase().startsWith(query.toLowerCase())) {
                sugestoes.add(apelido);
            }
        }
        return sugestoes;
    }

    public List<String> completeRacas(String query) {
        List<String> sugestoes = new ArrayList<>();
        for (String raca : gerarRacas()) {
            if (raca.toLowerCase().startsWith(query.toLowerCase())) {
                sugestoes.add(raca);
            }
        }
        return sugestoes;
    }


    public List<String> gerarRacas() {
        String[] prefixos = {"Nelore", "Baxter", "Zebu"};
        return gerarSequenciasAletorias(prefixos, 5000);
    }

    private List<String> gerarApelidos() {
        String[] prefixos = {"Mimosa", "Ze Bonitinho ", "Reprodutor"};
        return gerarSequenciasAletorias(prefixos, 5000);
    }


    public static List<String> gerarSequenciasAletorias(String[] prefixos, int tamanho) {
        List<String> retorno = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < tamanho; i++) {
            retorno.add(prefixos[random.nextInt(prefixos.length)] + random.nextInt(1000));
        }
        return retorno;
    }


    public List<Animal> getAnimais() {
        return this.animais;
    }

    public void setAnimais(List<Animal> animais) {
        this.animais = animais;
    }


    public Variedade[] getVariedades() {
        return Variedade.values();
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public List<String> getApelidos() {
        return apelidos;
    }

    public void setApelidos(List<String> apelidos) {
        this.apelidos = apelidos;
    }

    public boolean isDeveFazerUpload() {
        return deveFazerUpload;
    }

    public void setDeveFazerUpload(boolean deveFazerUpload) {
        this.deveFazerUpload = deveFazerUpload;
    }

    public UploadedFile getArquivo() {
        return arquivo;
    }

    public void setArquivo(UploadedFile arquivo) {
        this.arquivo = arquivo;
    }

    public List<String[]> getPreview() {
        return preview;
    }

    public void setPreview(List<String[]> preview) {
        this.preview = preview;
    }

    public boolean isDeveExibirBotaoPreview() {
        return deveExibirBotaoPreview;
    }

    public void setDeveExibirBotaoPreview(boolean deveExibirBotaoPreview) {
        this.deveExibirBotaoPreview = deveExibirBotaoPreview;
    }
}
